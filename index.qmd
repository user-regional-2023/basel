---
title: R/Basel 2023 
knitr:
  opts_chunk: 
    collapse: true
execute: 
  echo: false
page-layout: full
---

::: {.grid}

::: {.g-col-12}
[A useR! affiliated regional event on July 21st 2023]{.home-header}
:::

::: {.g-col-12}
![Basel | Switzerland](basel.jpeg){width=100%}
:::

::: {.g-col-12 .centre}
[This event recordings are now available on youtube](https://youtube.com/playlist?list=PLMtxz1fUYA5A2lLfwjIL0S43H20bw6JON)
:::

::: {.g-col-12 .g-col-md-4}
<div class="card h-100" >
<div class="card-body d-flex flex-column">
<p class="card-text">More information on the agenda  and themes for this one day event held in Basel on July 21st.</p>
<div style="margin-top: auto;"><a href="agenda.qmd" class="card-link heading-font">Agenda and themes</a></div>
</div>
</div>
:::

::: {.g-col-12 .g-col-md-4}
<div class="card h-100" >
<div class="card-body d-flex flex-column">
<p class="card-text">View updates from the organising committee.</p>
<div style="margin-top: auto;"><a href="posts.qmd" class="card-link second-color-link heading-font">Updates</a></div>
</div>
</div>
:::

::: {.g-col-12 .g-col-md-4}
<div class="card h-100" >
<div class="card-body d-flex flex-column">
<p class="card-text">R Community Events 2023 dashboard, where you can discover other events in the series.</p>
<div style="margin-top: auto;"><a href="https://rconf.gitlab.io/community-events-2023/" class="card-link third-color-link heading-font">App link</a></div>
</div>
</div>
:::

::: {.g-col-12 .g-col-md-4}
<div class="card h-100">
<div class="card-body d-flex flex-column">
<p class="card-text">Basel is well connected by train to European centres. The following link is to the official Basel tourist website, which has information on accommodation.</p>
<div style="margin-top: auto;"><a href="https://www.basel.com/en" class="card-link second-color-link heading-font">Visit Basel</a></div>
</div>
</div>
:::

::: {.g-col-12 .g-col-md-4}
<div class="card h-100">
<div class="card-body d-flex flex-column">
<p class="card-text">The event is being held at Roche's main campus. Details on the location and entry to the site are shared via this link.</p>
<div style="margin-top: auto;"><a href="venue.qmd" class="card-link third-color-link heading-font">Venue</a></div>
</div>
</div>
:::

::: {.g-col-12 .g-col-md-4}
<div class="card h-100">
<div class="card-body d-flex flex-column">
<p class="card-text">Have a question? Reach out to the organisors and let us help!</p>
<div style="margin-top: auto;"><a href="contact.qmd" class="card-link heading-font">Contact details</a></div>
</div>
</div>
:::

:::
