---
title: "Venue"
---

The one day event will be held in the Roche campus. More details will 
be provided closer to the event.


<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1346.225996327678!2d7.6079966764788685!3d47.55899353453716!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4791b9c8e27cec57%3A0x66afeae6b2c92633!2sRoche%20Tower%20(Building%201)!5e0!3m2!1sen!2sch!4v1682672963457!5m2!1sen!2sch" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>